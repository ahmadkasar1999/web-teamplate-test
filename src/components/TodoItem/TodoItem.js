// import React, { useState } from 'react';
// import { Menu } from 'semantic-ui-react';
// import './style.css';
// export default function Navbar() {
//   const [activeItem, setActiveItem] = useState('HOME');
//   const handleItemClick = (name) => setActiveItem(name);

//   return (
//     <div>
//       <Menu pointing secondary>
//         <Menu.Menu position='left'>
//           <Menu.Item name='LOGO' className='white' />
//         </Menu.Menu>
//         <Menu.Menu position='right'>
//           <Menu.Item className='grey' name='HOME' active={activeItem === 'HOME'} onClick={() => handleItemClick('HOME')} />
//           <Menu.Item className='grey' name='DOMAIN' active={activeItem === 'DOMAIN'} onClick={() => handleItemClick('DOMAIN')} />
//           <Menu.Item className='grey' name='HOSTING' active={activeItem === 'HOSTING'} onClick={() => handleItemClick('HOSTING')} />
//           <Menu.Item className='grey' name='CLIENTS' active={activeItem === 'CLIENTS'} onClick={() => handleItemClick('CLIENTS')} />
//           <Menu.Item className='grey' name='BLOG' active={activeItem === 'BLOG'} onClick={() => handleItemClick('BLOG')} />
//           <Menu.Item className='grey' name='SUPPORT' active={activeItem === 'SUPPORT'} onClick={() => handleItemClick('SUPPORT')} />
//         </Menu.Menu>
//       </Menu>
//     </div>
//   );
// }
